variable "environment" {
  default = "localdev"
  }
  
locals {
  environment = terraform.workspace == "default" ? var.environment : terraform.workspace
  }
  
output "environment" {
  value = local.environment
  }
  
